import java.util.concurrent.locks.ReentrantLock;
import java.time.Instant;

class MTCollatz implements Runnable {

    public volatile static int COUNTER = 2;
    public volatile static int N;
    ReentrantLock rl;
    String threadName;
    public volatile static int[] stoppingTimes;

    public MTCollatz(String threadName, ReentrantLock rl){
        this.threadName = threadName;
        this.rl = rl;
    }

    @Override
    public void run(){
        while (COUNTER <= N){
            // System.out.println("Thread- " + threadName + " is trying to acquire lock");
            boolean flag = rl.tryLock();
            if (flag){
                try {
                    // System.out.println("Thread- " + threadName + " has got lock");
                    collatzFunction(COUNTER);
                    COUNTER++;
                } finally{      
                    rl.unlock();
                    // System.out.println("Count of locks held by thread " + threadName + " - " + rl.getHoldCount());
                } 
            }else{
                // System.out.println("Thread- " + threadName + " not able to acquire lock.");
            }
        }
    }

    public void collatzFunction(int i){
        // System.out.println("In lockMethod, thread " + threadName + " is waiting to get lock");    
        rl.lock();
        try{
            Integer count = recursiveCollatz(i, 0);
            // System.out.println("Counter: " + COUNTER);
            // System.out.println("Count: "+ count);
            stoppingTimes[count]++;
        }finally {
            rl.unlock();
        }
    }

    public Integer recursiveCollatz(int i, int count){
        // System.out.println("bi:  "+i);
        count = count + 1;
        int fn = 0;
        if (i == 1){
            fn = count;
        }else if (i % 2 != 0){
            fn = recursiveCollatz( 3*i +1, count); //f(n) = 3n+1
        }else {
            fn = recursiveCollatz(i/2, count); // f(n) = n/2
        }
        return fn;
    }
    public static void main(String[] args) {
        Instant startInstant = Instant.now();
        N = Integer.parseInt(args[0]);
        int t = Integer.parseInt(args[1]);
        // N = 100000;
        // int t = 8;
        //N = 10^6 has 524 steps
        int maxSteps = 1000 ;
        stoppingTimes = new int[maxSteps];
        Thread threads[] = new Thread[t];
        ReentrantLock rl = new ReentrantLock();
        for (int i = 0; i < t; i++){
            threads[i] = new Thread(new MTCollatz("Thread-" + i, rl));
            threads[i].start();
        }

        for (int i = 0; i < t; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i =0; i<stoppingTimes.length; i++){
		    System.out.println(i+1 + "," + stoppingTimes[i]);
        }
        Instant endInstant = Instant.now();
		long timeElapsed = endInstant.toEpochMilli()- startInstant.toEpochMilli();
        System.out.println(N + ","+ t + "," + timeElapsed);
    }
}