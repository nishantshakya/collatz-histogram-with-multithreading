# Multi-Threaded Collatz Histogram Generator

Application of java thread synchronization to prevent race condition.

## Input Arguments
N = Integer
t = Number of threads

Collatz conjecture: https://en.wikipedia.org/wiki/Collatz_conjecture

## Usage

```
java MTCollatz N t
java MTCollatz 10000 4
```


## License
[MIT](https://choosealicense.com/licenses/mit/)